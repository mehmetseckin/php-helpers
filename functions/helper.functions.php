<?php

/**
 * Safe redirect function, avoids headers already sent.
 */
function redirect($uri = '') {
    $url = base_url($uri);
try {
    if (!headers_sent()) {
            // Headers aren't sent, server-side redirecting !
            header('Location: ' . $url);
            exit;
        } else {
            throw new Exception();
        }
    } catch (Exception $ex) {
    // Headers already sent!! Javascript?
    echo '<script type="text/javascript">'
    . 'window.location.href="' . $url . '";'
    . '</script>'
    // Javascript disabled, try metatags.
    . '<noscript>'
    . '<meta http-equiv="refresh" content="0;url=' . $url . '" />'
    . '</noscript>';
    exit;
    }    
}

/**
 * Try and get the client's IP address.
 */
function getClientIPAddress() {
	$ip;
	if (getenv("HTTP_CLIENT_IP"))
		$ip = getenv("HTTP_CLIENT_IP");
	else if (getenv("HTTP_X_FORWARDED_FOR"))
		$ip = getenv("HTTP_X_FORWARDED_FOR");
	else if (getenv("REMOTE_ADDR"))
		$ip = getenv("REMOTE_ADDR");
	else
		$ip = "UNKNOWN";
	return $ip;
}