<?php

/**
 * RequestManager is a helper class for storing and accessing GET and/or POST variables
 * in an Object-Oriented way.
 *
 * Author: Mehmet Seckin (seckin92@gmail.com)
 */
 
class RequestManager {
    private $data;
    public function __construct($type = "REQUEST") {
        switch($type) {
            case  "REQUEST":
                $this->data = $_REQUEST;
                break;
            case "POST":
                $this->data = $_POST;
                break;
            case "GET":
                $this->data = $_GET;
                break;
        }
    }    
    
    public function __get($key) {
        if(isset($this->data[$key]))
            return $this->data[$key];
        else
            return null;
    }
    
    public function __set($key, $value ="") {
        $this->data[$key] = $value;
    }
    
    public function destroy() {
        foreach(array_keys($this->data) as $key) {
            unset($this->data[$key]);
        }
    }
} 