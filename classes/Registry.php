<?php

/**
 * Registry is a generic class to store and use variables
 * in an Object-Oriented way.
 *
 * Author: Mehmet Seckin (seckin92@gmail.com)
 */

class Registry {
    protected $data = array();
    
   // Overriding the magic get accessor to respond our arbitrary variable calls.
   public function &__get($varName){
       if (!array_key_exists($varName, $this->data)) {
            return false;
        } else {
            $data = $this->data[$varName];
            return $data;
        }
    }

   // Overriding the magic set accessor so it lets us use arbitrary variables.
   public function __set($varName,$value){
      $this->data[$varName] = $value;    
   }
   
    public function import($incomingData = array()) {
       $this->data = array_merge($this->data, $incomingData);
    }
    
    public function all() {
        return $this->data;
    }
}