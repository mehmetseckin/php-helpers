<?php
/**
 * SessionManager is a helper class for storing and accessing session variables
 * in an Object-Oriented way.
 *
 * Author: Mehmet Seckin (seckin92@gmail.com)
 */
 
class SessionManager {
    private $sessionId;
    public function __construct() {
        if(session_id() == "")
            session_start();
        $this->sessionId = session_id();
    }    
    
    public function __get($key) {
        if(isset($_SESSION[$key]))
            return $_SESSION[$key];
        else
            return null;
    }
    
    public function __set($key, $value ="") {
        $_SESSION[$key] = $value;
    }
    
    public function destroy() {
        foreach(array_keys($_SESSION) as $key) {
            unset($_SESSION[$key]);
        }
        session_destroy();
    }
    
    public function import($incomingData = array()) {
       $_SESSION = array_merge($_SESSION, $incomingData);
    } 
    
    public function all() {
        return $_SESSION;
    }
    
    public function __toString() {
        return $this->sessionId;
    }
} 